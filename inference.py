import uvicorn
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException
import numpy as np
import scipy 
import pandas as pd

load_dotenv()

app = FastAPI()


def load_sparse_csr(filename: str):
    loader = np.load(filename)
    return scipy.sparse.csr_matrix((loader['data'],
                                    loader['indices'],
                                    loader['indptr']),
                                   shape=loader['shape'])


class Model:
    def __init__(self, model_name: str, model_stage: str):
        self.model = mlflow.pyfunc.load_model(f'models:/{model_name}/{model_stage}')

    def predictions(self, data):
        predictions = self.model.predict(data)
        return predictions


model = Model("boar_game_model", "Staging")


@app.post('/invocations')
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith('.npz'):
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())
        data = load_sparse_csr(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))

    else:
        raise HTTPException(status_code=400, detail='Invalid file format. Only npz file accepted')

if os.getenv('AWS_ACCESS_KEY_ID') is None or os.getenv('AWS_SECRET_ACCESS_KEY') is None:
    exit(1)
